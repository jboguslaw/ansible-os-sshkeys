Role Name
=========

ADd/Rmmove SSH public keys from users

Requirements
------------

N/A

Role Variables
--------------

	- users_list - yml file with SSH keys, look at vars/users_example.yml 

tags:
	- addsshkey - to add user keys

Dependencies
------------

N/A

Example Playbook
----------------

```yaml
- name: SSH Keys OS configation
  hosts: all
  #strategy: debug
  gather_facts: yes
  become: yes
  roles:
    - { role: ansible-os-sshkeys, tags: [ 'all' ] }
```

License
-------

BSD

Author Information
------------------

Jakub K. Boguslaw <jboguslaw@gmail.com> please inform me if you find any error/mistake
